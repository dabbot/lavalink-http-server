FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY ./target/release/lavalink-http-server .
COPY ./config.toml .
EXPOSE 14001
ENTRYPOINT ["/webapp"]
