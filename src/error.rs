use http::{
    Error as HttpError,
    uri::InvalidUri,
};
use hyper::error::Error as HyperError;
use lavalink_futures::{
    lavalink::Error as LavalinkError,
    Error as LavalinkFuturesError,
};
use queue::Error as LavalinkQueueError;
use redis_async::error::Error as RedisError;
use serde_json::Error as JsonError;
use std::{
    cell::BorrowMutError,
    env::VarError,
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    io::Error as IoError,
    net::AddrParseError,
    num::ParseIntError,
    result::Result as StdResult,
};
use tokio::timer::Error as TokioTimerError;
use toml::de::Error as TomlDeError;

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    AddrParse(AddrParseError),
    BorrowMut(BorrowMutError),
    Http(HttpError),
    Hyper(HyperError),
    InvalidUri(InvalidUri),
    Io(IoError),
    Json(JsonError),
    Lavalink(LavalinkError),
    LavalinkFutures(LavalinkFuturesError),
    LavalinkQueue(LavalinkQueueError),
    None,
    ParseInt(ParseIntError),
    Redis(RedisError),
    TokioTimer(TokioTimerError),
    TomlDe(TomlDeError),
    Var(VarError),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_str(self.description())
    }
}

impl StdError for Error {
    fn description(&self) -> &str {
        use self::Error::*;

        match *self {
            AddrParse(ref inner) => inner.description(),
            BorrowMut(ref inner) => inner.description(),
            Http(ref inner) => inner.description(),
            Hyper(ref inner) => inner.description(),
            InvalidUri(ref inner) => inner.description(),
            Io(ref inner) => inner.description(),
            Json(ref inner) => inner.description(),
            Lavalink(ref inner) => inner.description(),
            LavalinkFutures(ref inner) => inner.description(),
            LavalinkQueue(ref inner) => inner.description(),
            None => "No value found",
            ParseInt(ref inner) => inner.description(),
            Redis(ref inner) => inner.description(),
            TokioTimer(ref inner) => inner.description(),
            TomlDe(ref inner) => inner.description(),
            Var(ref inner) => inner.description(),
        }
    }
}

impl From<AddrParseError> for Error {
    fn from(err: AddrParseError) -> Self {
        Error::AddrParse(err)
    }
}

impl From<BorrowMutError> for Error {
    fn from(err: BorrowMutError) -> Self {
        Error::BorrowMut(err)
    }
}

impl From<HttpError> for Error {
    fn from(err: HttpError) -> Self {
        Error::Http(err)
    }
}

impl From<HyperError> for Error {
    fn from(err: HyperError) -> Self {
        Error::Hyper(err)
    }
}

impl From<InvalidUri> for Error {
    fn from(err: InvalidUri) -> Self {
        Error::InvalidUri(err)
    }
}

impl From<IoError> for Error {
    fn from(err: IoError) -> Self {
        Error::Io(err)
    }
}

impl From<JsonError> for Error {
    fn from(err: JsonError) -> Self {
        Error::Json(err)
    }
}

impl From<LavalinkError> for Error {
    fn from(err: LavalinkError) -> Self {
        Error::Lavalink(err)
    }
}

impl From<LavalinkFuturesError> for Error {
    fn from(err: LavalinkFuturesError) -> Self {
        Error::LavalinkFutures(err)
    }
}

impl From<LavalinkQueueError> for Error {
    fn from(err: LavalinkQueueError) -> Self {
        Error::LavalinkQueue(err)
    }
}

impl From<ParseIntError> for Error {
    fn from(err: ParseIntError) -> Self {
        Error::ParseInt(err)
    }
}

impl From<RedisError> for Error {
    fn from(err: RedisError) -> Self {
        Error::Redis(err)
    }
}

impl From<TokioTimerError> for Error {
    fn from(err: TokioTimerError) -> Self {
        Error::TokioTimer(err)
    }
}

impl From<TomlDeError> for Error {
    fn from(err: TomlDeError) -> Self {
        Error::TomlDe(err)
    }
}

impl From<VarError> for Error {
    fn from(err: VarError) -> Self {
        Error::Var(err)
    }
}
