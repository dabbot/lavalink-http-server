use crate::config::Config;
use hyper::client::{Client, HttpConnector};
use lavalink_futures::nodes::NodeManager;
use queue::QueueClient;
use redis_async::client::PairedConnection;
use reqwest::r#async::Client as ReqwestClient;
use std::sync::Arc;

pub struct ApplicationState {
    pub config: Arc<Config>,
    pub hyper: Arc<Client<HttpConnector>>,
    pub node_manager: Arc<NodeManager>,
    pub queue: QueueClient,
    pub redis: Arc<PairedConnection>,
    pub reqwest: Arc<ReqwestClient>,
}
