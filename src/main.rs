#![feature(
    async_await,
    await_macro,
    futures_api,
)]

#[macro_use] extern crate log;
#[macro_use] extern crate redis_async;
#[macro_use] extern crate serde_derive;

mod config;
mod error;
mod node_manager;
mod router;
mod state;
mod utils;

pub use crate::error::{Error, Result};

use cache::Cache;
use crate::{
    config::Config,
    node_manager::CreationState,
    state::ApplicationState,
};
use futures::future::{FutureExt, TryFutureExt};
use hyper::{
    client::Client as HyperClient,
    rt::Future as Future01,
    service,
    Server,
};
use queue::QueueClient;
use redis_async::client as redis_client;
use reqwest::r#async::Client as ReqwestClient;
use std::{
    env,
    sync::Arc,
};

fn main() {
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "debug");
    }

    env_logger::init();

    tokio::run(try_main().map_err(|why| {
        error!("Error running loop: {:?}", why);
    }));
}

fn try_main() -> impl Future01<Item = (), Error = Error> + Send {
    let config = Arc::new(Config::new("./config.toml").unwrap());
    let config2 = Arc::clone(&config);
    let redis_addr = config.redis.addr().unwrap();
    let hyper = Arc::new(HyperClient::new());
    let hyper2 = Arc::clone(&hyper);
    let reqwest = Arc::new(ReqwestClient::new());
    let reqwest2 = Arc::clone(&reqwest);

    redis_client::paired_connect(&redis_addr).from_err().and_then(move |redis| {
        let redis = Arc::new(redis);
        let cache = Cache::new(Arc::clone(&redis));
        let queue = QueueClient::new(
            Arc::clone(&reqwest2),
            config.queue.address.clone(),
        );

        let state = CreationState {
            cache,
            config,
            hyper: Arc::clone(&hyper),
            redis: Arc::clone(&redis),
            reqwest: reqwest2,
            queue,
        };
        node_manager::create(state)
            .boxed()
            .compat()
            .from_err()
            .map(|node_manager| (node_manager, redis))
    }).map(move |(node_manager, redis)| {
        let addr = config2.server.address.parse().unwrap();
        let queue = QueueClient::new(
            Arc::clone(&reqwest),
            config2.queue.address.clone(),
        );

        let state = Arc::new(ApplicationState {
            config: Arc::clone(&config2),
            hyper: Arc::clone(&hyper2),
            node_manager: Arc::clone(&node_manager),
            redis,
            reqwest,
            queue,
        });

        let service = move || {
            let state2 = state.clone();

            service::service_fn(move |req| {
                router::handle(req, Arc::clone(&state2))
            })
        };

        let server = Server::bind(&addr).serve(service).map_err(|why| {
            warn!("Error running http server: {:?}", why);
        });
        info!(
            "HTTP server listening on {}",
            config2.server.address,
        );

        tokio::spawn(server);
    })
}
