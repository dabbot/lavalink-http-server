use crate::Result;
use std::{
    fs::File,
    io::Read,
    net::SocketAddr,
    path::Path,
    str::FromStr,
};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Config {
    pub discord: DiscordConfig,
    pub nodes: Vec<LavalinkNodeConfig>,
    pub queue: QueueConfig,
    pub redis: RedisConfig,
    pub server: ServerConfig,
}

impl Config {
    pub fn new(path: impl AsRef<Path>) -> Result<Self> {
        Self::_new(path.as_ref())
    }

    fn _new(path: &Path) -> Result<Self> {
        let mut file = File::open(path)?;
        let mut contents = Vec::new();
        file.read_to_end(&mut contents)?;

        toml::from_slice(&contents).map_err(From::from)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct DiscordConfig {
    pub num_shards: u64,
    pub user_id: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct LavalinkNodeConfig {
    pub host: String,
    pub port: u16,
    pub password: String,
}

impl LavalinkNodeConfig {
    pub fn addr(&self) -> String {
        format!("{}:{}", self.host, self.port)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct QueueConfig {
    pub address: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct RedisConfig {
    pub address: String,
}

impl RedisConfig {
    pub fn addr(&self) -> Result<SocketAddr> {
        SocketAddr::from_str(&self.address).map_err(From::from)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ServerConfig {
    pub address: String,
    pub timeout: u64,
}
