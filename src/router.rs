use crate::{
    error::{Error, Result},
    state::ApplicationState,
};
use futures::{
    compat::Future01CompatExt,
    FutureExt,
    TryFutureExt,
};
use http::request::Parts;
use hyper::{
    header::{CONTENT_LENGTH, HeaderValue},
    rt::{Future as Future01, Stream as Stream01},
    Body,
    Method,
    Request,
    Response,
    StatusCode,
};
use lavalink::{
    model::*,
    rest::hyper::LavalinkRestRequester,
};
use serde::de::DeserializeOwned;
use serde_json;
use std::sync::Arc;

pub fn handle(
    req: Request<Body>,
    state: Arc<ApplicationState>,
) -> impl Future01<Item = Response<Body>, Error = Error> + Send {
    inner(req, state).boxed().compat()
}

async fn inner(
    req: Request<Body>,
    state: Arc<ApplicationState>,
) -> Result<Response<Body>> {
    match await!(perform(req, state)) {
        Ok(res) => Ok(res),
        Err(Error::None) => {
            let mut resp = Response::new(Body::from("404 not found"));
            *resp.status_mut() = StatusCode::NOT_FOUND;

            Ok(resp)
        },
        Err(Error::Json(why)) => {
            let mut resp = Response::new(Body::from(format!("{:?}", why)));
            *resp.status_mut() = StatusCode::BAD_REQUEST;

            Ok(resp)
        },
        Err(Error::ParseInt(why)) => {
            let mut resp = Response::new(Body::from(format!("{:?}", why)));
            *resp.status_mut() = StatusCode::BAD_REQUEST;

            Ok(resp)
        }
        Err(why) => {
            let mut resp = Response::new(Body::from(format!("{:?}", why)));
            *resp.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;

            warn!("Err with request: {:?}", why);

            Ok(resp)
        },
    }
}

async fn perform(req: Request<Body>, state: Arc<ApplicationState>) -> Result<Response<Body>> {
    let (parts, req_body) = req.into_parts();
    let Parts {
        method: req_method,
        uri: req_uri,
        version: _,
        headers: _,
        extensions: _,
        ..
    } = parts;

    match (req_method, req_uri.path()) {
        (Method::GET, "/") => {
            let mut resp = Response::new(Body::from("hello"));
            *resp.status_mut() = StatusCode::OK;
            resp.headers_mut().insert(CONTENT_LENGTH, HeaderValue::from(5u64));

            Ok(resp)
        },
        (_, "/") => method_not_allowed(),
        (Method::POST, "/equalizer") => {
            let node_manager = Arc::clone(&state.node_manager);

            let equalizer = await!(deser_body::<Equalizer>(req_body))?;
            let guild_id = equalizer.guild_id.parse()?;

            let player = node_manager.player(guild_id, None)?;
            player.write().equalizer(equalizer.bands)?;

            Ok(Response::new(Body::from("")))
        },
        (_, "/equalizer") => method_not_allowed(),
        (Method::POST, "/pause") => {
            let node_manager = Arc::clone(&state.node_manager);

            let pause = await!(deser_body::<Pause>(req_body))?;

            let guild_id = pause.guild_id.parse()?;

            let player = node_manager.player(guild_id, None)?;
            player.write().pause(pause.pause)?;

            Ok(Response::new(Body::from("")))
        },
        (_, "/pause") => method_not_allowed(),
        (Method::POST, "/play") => {
            debug!("state: {:?}", state.node_manager.player_manager);
            let node_manager = Arc::clone(&state.node_manager);

            let play = await!(deser_body::<Play>(req_body)).map_err(|why| {
                warn!("Err deserializing: {:?}", why);

                why
            })?;

            debug!("Play data: {:?}", play);

            let guild_id = play.guild_id.parse()?;

            debug!("Guild ID parsed");

            debug!("Getting player");
            let player = node_manager.player(guild_id, None)?;
            debug!("Sending play data");

            let is_playing = player.read().track.is_some();

            if is_playing {
                debug!("Adding track to queue");

                await!(state.queue.add_track(
                    play.guild_id.clone(),
                    play.track.clone(),
                ))?;

                debug!("Queued song");

                Response::builder()
                    .status(202)
                    .body(Body::from("false"))
                    .map_err(From::from)
            } else {
                debug!("Playing track as requested");

                {
                    let mut player = player.write();

                    player.play(
                        &play.track,
                        play.start_time,
                        play.end_time,
                    )?;

                    player.track = Some(play.track.clone());
                }

                debug!("Sent play data");

                Response::builder()
                    .status(201)
                    .body(Body::from("true"))
                    .map_err(From::from)
            }
        },
        (_, "/play") => method_not_allowed(),
        (Method::POST, "/player") => {
            let node_manager = Arc::clone(&state.node_manager);

            #[derive(Clone, Debug, Deserialize, Serialize)]
            struct PlayerPayload {
                guild_id: String,
            }

            debug!("Deserializing player body");
            let body = await!(deser_body::<PlayerPayload>(req_body))?;
            debug!("Deserialized player body: {:?}", body);

            debug!("Parsing guild ID");
            let id = body.guild_id.parse::<u64>()?;
            debug!("Parsed guild ID");

            debug!("Getting player for guild {}", id);
            let player = node_manager.player(id, None)?;
            debug!("Got player for guild");

            debug!("Serializing player state");
            let payload = serde_json::to_vec_pretty(&*player.read())?;
            debug!("Serialized player state");

            Ok(Response::new(Body::from(payload)))
        }
        (_, "/player") => method_not_allowed(),
        (Method::GET, "/search") => {
            let (node_rest_host, node_password) = {
                let manager = &state.node_manager;
                let nodes = manager.nodes.lock();
                let mut iter = nodes.iter();
                let node = iter.next().ok_or(Error::None)?;
                let node = node.1.lock();

                (format!("http://{}", node.host.clone()), node.password.clone())
            };

            debug!("Getting search request URI");
            let params = req_uri.query().ok_or(Error::None)?;
            debug!("Got search request URI: {:?}", params);
            let first = if let Some(pos) = params.find('&') {
                &params[..pos]
            } else {
                &params
            };
            let value = first.split_at(first.find('=').ok_or(Error::None)?).1;
            let value = &value[1..];

            let client = state.hyper.clone();
            debug!("Loading tracks");
            let tracks = match await!(client.load_tracks(
                node_rest_host,
                node_password,
                value,
            ).compat()) {
                Ok(tracks) => tracks,
                Err(why) => {
                    warn!("Err loading tracks: {:?}", why);

                    return Ok(Response::new(Body::from("")));
                },
            };
            debug!("Loaded tracks");

            let ser = serde_json::to_vec(&tracks)?;
            debug!("Parsed tracks to bytes");

            Ok(Response::new(Body::from(ser)))
        },
        (_, "/search") => method_not_allowed(),
        (Method::POST, "/seek") => {
            let node_manager = Arc::clone(&state.node_manager);

            let seek = await!(deser_body::<Seek>(req_body))?;
            let guild_id = seek.guild_id.parse()?;

            let player = node_manager.player(guild_id, None)?;
            player.write().seek(seek.position)?;

            Ok(Response::new(Body::from("")))
        },
        (_, "/seek") => method_not_allowed(),
        (Method::POST, "/skip") => {
            debug!("Received skip, deserializing");
            let node_manager = Arc::clone(&state.node_manager);

            #[derive(Debug, Deserialize)]
            struct Skip {
                pub guild_id: u64,
            }

            let skip = await!(deser_body::<Skip>(req_body))?;
            let guild_id = skip.guild_id;

            debug!("Getting audio manager");
            let player = node_manager.player(guild_id, None)?;
            debug!("Got audio manager");

            debug!("Stopping on skip");
            player.write().stop()?;
            debug!("Stopped on skip");

            Ok(Response::new(Body::from("")))
        },
        (_, "/skip") => method_not_allowed(),
        (Method::POST, "/stop") => {
            debug!("Received stop, deserializing");
            let node_manager = Arc::clone(&state.node_manager);

            let stop = await!(deser_body::<Stop>(req_body))?;
            debug!("Deserialized stop: {:?}", stop);
            debug!("Parsing guild id");
            let guild_id = stop.guild_id.parse()?;
            debug!("Parsed guild id");

            {
                debug!("Getting audio manager");
                let player = node_manager.player(guild_id, None)?;
                debug!("Got audio manager, destroying");
                player.write().destroy()?;
                debug!("Destroyed audio manager");
            }

            node_manager.remove_player(&guild_id);

            Ok(Response::new(Body::from("")))
        },
        (_, "/stop") => method_not_allowed(),
        (Method::POST, "/voice-update") => {
            debug!("Got voice update");
            let node_manager = Arc::clone(&state.node_manager);

            debug!("Deserializing voice update body");
            let voice_update = await!(deser_body::<VoiceUpdate>(req_body))?;
            debug!("Deserialized voice update body");
            let guild_id = voice_update.guild_id.parse()?;
            debug!("Parsed guild id");

            debug!("Getting audio player for guild");
            let player = node_manager.player(guild_id, None)?;
            debug!("Got audio player for guild");

            debug!("Parsing voice update to bytes");
            let update = serde_json::to_string(&voice_update)?;
            debug!("Sending bytes to player");
            player.write().send(update)?;
            debug!("Sent bytes to player");

            Ok(Response::new(Body::from("")))
        },
        (_, "/voice-update") => method_not_allowed(),
        (Method::POST, "/volume") => {
            let node_manager = Arc::clone(&state.node_manager);

            let volume = await!(deser_body::<Volume>(req_body))?;
            let guild_id = volume.guild_id.parse()?;

            let player = node_manager.player(guild_id, None)?;

            player.write().volume(volume.volume)?;

            Ok(Response::new(Body::from("")))
        },
        (_, "/volume") => method_not_allowed(),
        _ => {
            let mut resp = Response::new(Body::from("404"));
            *resp.status_mut() = StatusCode::NOT_FOUND;
            resp.headers_mut().insert(CONTENT_LENGTH, HeaderValue::from(3u64));

            Ok(resp)
        },
    }
}

async fn deser_body<T: DeserializeOwned>(body: Body) -> Result<T> {
    let chunks = await!(body.concat2().compat())?;

    serde_json::from_slice::<T>(&chunks).map_err(From::from)
}

fn method_not_allowed() -> Result<Response<Body>> {
    let mut resp = Response::new(Body::from("Method not allowed"));
    *resp.status_mut() = StatusCode::METHOD_NOT_ALLOWED;

    Ok(resp)
}
