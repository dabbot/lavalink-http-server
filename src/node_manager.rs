use cache::{
    model::LoopMode,
    Cache,
};
use crate::{
    config::{Config, LavalinkNodeConfig},
    error::Result,
    utils,
};
use futures::{
    compat::Future01CompatExt,
    channel::mpsc::{self, UnboundedReceiver},
    prelude::*,
};
use hyper::client::{Client as HyperClient, HttpConnector};
use lavalink::model::{Event, IncomingMessage};
use lavalink_futures::{
    nodes::{Node, NodeConfig, NodeManager, NodeMessage},
    player::AudioPlayerManager,
};
use queue::QueueClient;
use parking_lot::Mutex;
use redis_async::{
    client::PairedConnection,
};
use reqwest::r#async::Client as ReqwestClient;
use std::{
    sync::Arc,
    time::{Duration, Instant},
};
use tokio::timer::Delay;

pub struct CreationState {
    pub cache: Cache,
    pub config: Arc<Config>,
    pub hyper: Arc<HyperClient<HttpConnector>>,
    pub queue: QueueClient,
    pub redis: Arc<PairedConnection>,
    pub reqwest: Arc<ReqwestClient>,
}

pub async fn create(state: CreationState) -> Result<Arc<NodeManager>> {
    debug!("Creating node manager");

    let state = Arc::new(state);

    let timeout = Duration::from_secs(state.config.server.timeout);
    let manager = Arc::new(NodeManager::new(timeout));
    let audio_manager = Arc::clone(&manager.player_manager);

    for node in state.config.nodes.clone() {
        debug!("Connecting to node `{}`", node.addr());

        tokio::spawn(node_spawner(
            Arc::clone(&manager),
            node,
            Arc::clone(&state),
            Arc::clone(&audio_manager),
        ).map_err(|why| warn!("Err with node spawner: {:?}", why)).boxed().compat());
    }

    debug!("Connected to all lavalink nodes");

    Ok(manager)
}

async fn node_spawner(
    manager: Arc<NodeManager>,
    node_config: LavalinkNodeConfig,
    state: Arc<CreationState>,
    audio_manager: Arc<Mutex<AudioPlayerManager>>,
) -> Result<()> {
    let (tx, mut rx) = mpsc::unbounded();

    if let Err(why) = tx.unbounded_send(()) {
        error!("Err sending initial node spawn: {:?}", why);
    }

    while let Some(()) = await!(rx.next()) {
        loop {
            let manager2 = Arc::clone(&manager);
            let node_config2 = node_config.clone();
            let state = Arc::clone(&state);
            let audio_manager2 = Arc::clone(&audio_manager);

            match await!(create_node(manager2, node_config2, state, audio_manager2)) {
                Ok(node) => {
                    debug!("Respawned node: {:?}", node);

                    debug!("Updating senders for node's players");

                    let mut updated = 0;

                    for player in audio_manager.lock().players().values() {
                        if player.read().node.lock().host == node_config.addr() {
                            *player.write().sender_mut() = node.lock().tx().clone();

                            updated += 1;
                        }
                    }

                    debug!("Updated {} players for node", updated);

                    break;
                },
                Err(why) => {
                    warn!("Err spawning node: {:?}", why);
                    debug!("Sleeping one second");

                    await!(Delay::new(Instant::now() + Duration::from_secs(1)).compat())?;
                },
            }
        }
    }

    error!("Node spawner ended???");

    Ok(())
}

async fn create_node(
    manager: Arc<NodeManager>,
    node: LavalinkNodeConfig,
    state: Arc<CreationState>,
    audio_manager: Arc<Mutex<AudioPlayerManager>>,
) -> Result<Arc<Mutex<Node>>> {
    let addr = node.addr();

    let (node, rx) = {
        await!(manager.add_node(NodeConfig {
            host: addr,
            num_shards: state.config.discord.num_shards,
            password: node.password,
            user_id: state.config.discord.user_id.clone(),
        }))?
    };

    let audio_manager = Arc::clone(&audio_manager);
    let state = Arc::clone(&state);

    tokio::spawn(handle_msgs(
        rx,
        Arc::clone(&state),
        Arc::clone(&audio_manager),
    ).map_err(|why| {
        warn!("Err with node handle messages: {:?}", why);
    }).boxed().compat());

    debug!("Connected to node!");

    Ok(node)
}

async fn handle_msgs(
    mut rx: UnboundedReceiver<NodeMessage>,
    state: Arc<CreationState>,
    audio_manager: Arc<Mutex<AudioPlayerManager>>,
) -> Result<()> {
    while let Some(msg) = await!(rx.next()) {
        match msg {
            NodeMessage::Bytes(bytes) => {
                trace!("String representation: {}", String::from_utf8(bytes.clone()).unwrap());

                let msg = match serde_json::from_slice::<IncomingMessage>(&bytes) {
                    Ok(msg) => msg,
                    Err(why) => {
                        warn!(
                            "Err deserializing incoming message {:?}: {:?}",
                            bytes,
                            why,
                        );

                        return Ok(());
                    },
                };

                utils::spawn(handle_msg(
                    msg,
                    Arc::clone(&state),
                    Arc::clone(&audio_manager),
                ));
            },
        }
    }

    error!("Handle msgs ended");

    Ok(())
}

async fn handle_msg(
    msg: IncomingMessage,
    state: Arc<CreationState>,
    manager: Arc<Mutex<AudioPlayerManager>>,
) -> Result<()> {
    match msg {
        IncomingMessage::Event(Event::TrackEnd(end)) => {
            debug!("Handling track end");

            debug!("Parsing guild ID");
            let guild_id_num = end.guild_id.parse()?;
            debug!("Parsed guild ID");

            debug!("Getting player state for guild {}", guild_id_num);
            let player = match manager.lock().get(&guild_id_num) {
                Some(player) => player,
                None => return Ok(()),
            };
            debug!("Got player state");

            let guild_id = end.guild_id.clone();

            debug!("Checking loop mode for {}", guild_id_num);

            match await!(state.cache.get_loop_mode(guild_id_num)) {
                Ok(Some(LoopMode::Queue)) => {
                    debug!("Loop mode for {}: queue", guild_id_num);

                    match await!(state.queue.add_track(end.guild_id.clone(), end.track)) {
                        Ok(_) => {
                            debug!("Looping: added to back of queue for {}", guild_id);
                        },
                        Err(why) => {
                            warn!("Err adding to back of queue for {}: {:?}", guild_id, why);
                        },
                    }
                },
                Ok(Some(LoopMode::Song)) => {
                    debug!("Loop mode for {}: song", guild_id_num);

                    let mut writer = player.write();
                    writer.play(&end.track, None, None)?;
                    writer.track = Some(end.track.clone());

                    utils::new_song(&state, guild_id_num, end.track);

                    return Ok(());
                }
                Ok(Some(LoopMode::Off)) | Ok(None) => {
                    debug!("Loop mode for {}: none", guild_id_num);
                },
                Err(why) => {
                    warn!("Err getting guild LHS player for {}: {:?}", guild_id, why);
                },
            };

            debug!("Handled loop mode");
            debug!("Popping next song off queue");

            let song = match await!(state.queue.pop(guild_id)) {
                Ok(Some(song)) => song,
                Ok(None) => {
                    debug!("No song to pop off queue");

                    return Ok(());
                },
                Err(why) => {
                    warn!(
                        "Err popping queue for {}: {:?}",
                        end.guild_id,
                        why,
                    );

                    return Ok(());
                },
            };
            debug!("Got a song off the queue");

            debug!("Playing next song: {:?}", song);

            {
                let mut writer = player.write();

                writer.play(&song.track_base64, None, None)?;
                writer.track = Some(song.track_base64.clone());
            }

            utils::new_song(&state, guild_id_num, song.track_base64);
        },
        IncomingMessage::Event(Event::TrackException(exception)) => {
            warn!("Track exception: {:?}", exception);
        },
        IncomingMessage::Event(Event::TrackStuck(stuck)) => {
            trace!("Track stuck: {:?}", stuck);
        },
        IncomingMessage::Event(Event::WebSocketClosed(ended)) => {
            debug!("Discord voice socket closed: {:?}", ended);
        },
        IncomingMessage::PlayerUpdate(update) => {
            debug!("Player update: {:?}", update);
        },
        IncomingMessage::Stats(stats) => {
            debug!("Stats: {:?}", stats);
        },
    }

    Ok(())
}
