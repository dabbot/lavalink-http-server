use crate::{
    error::Result,
    node_manager::CreationState,
};
use futures::future::{Future, FutureExt, TryFutureExt};
use std::sync::Arc;

pub fn new_song(
    state: &Arc<CreationState>,
    guild_id: u64,
    track: String,
) {
    #[derive(Debug, Serialize)]
    struct SongPlayed {
        guild_id: u64,
        track: String,
    }

    debug!("Played next song");

    let payload = SongPlayed {
        guild_id,
        track,
    };

    debug!("Sending song ID to lavalink:from");

    match serde_json::to_vec(&payload) {
        Ok(bytes) => {
            state.redis.send_and_forget(resp_array![
                "RPUSH",
                "lavalink:from",
                bytes
            ]);
        },
        Err(why) => {
            warn!(
                "Err serializing lavalink:from payload: {:?}; {:?}",
                payload,
                why,
            );
        },
    }

    debug!("Sent song ID to lavalink:from");
}

pub fn spawn(future: impl Future<Output = Result<()>> + Send + 'static) {
    tokio::spawn(future.map_err(|why| {
        warn!("Err with future: {:?}", why);
    }).boxed().compat());
}
